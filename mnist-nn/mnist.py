from datetime import datetime
from packaging import version
import numpy as np

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import datasets, layers, models

config = tf.compat.v1.ConfigProto(gpu_options = 
                         tf.compat.v1.GPUOptions(per_process_gpu_memory_fraction=0.8)
# device_count = {'GPU': 1}
)
config.gpu_options.allow_growth = True
session = tf.compat.v1.Session(config=config)
tf.compat.v1.keras.backend.set_session(session)

print("Hello from TensorFlow: " + tf.__version__)
print("TF build with CUDA: ",tf.test.is_built_with_cuda())
print("Num GPUs Available: ", len(tf.config.experimental.list_physical_devices("GPU")))
print("")

num_classes = 10
input_shape = (28, 28, 1)

(train_images, train_labels), (test_images, test_labels) = datasets.mnist.load_data()

# Scale images to the [0, 1] range
train_images = train_images.astype("float32") / 255
test_images = test_images.astype("float32") / 255

print("x_train shape:", train_images.shape)
print(train_images.shape[0], "train samples")
print(train_images.shape[0], "test samples")

train_images = np.expand_dims(train_images, -1)
test_images = np.expand_dims(test_images, -1)

# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(train_labels, num_classes)
y_test = keras.utils.to_categorical(test_labels, num_classes)

model = tf.keras.Sequential([
  tf.keras.layers.InputLayer(input_shape=(28, 28, 1)),
  tf.keras.layers.MaxPool2D((2,2)),
  tf.keras.layers.Conv2D(6, 3),
  tf.keras.layers.MaxPool2D((4,4)),
  tf.keras.layers.Flatten(),
  tf.keras.layers.Dense(10)
])

model.compile(optimizer='adam',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])

logdir="logs/fit/" + datetime.now().strftime("%Y%m%d  -%H%M%S")
tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir)

history = model.fit(test_images, test_labels, epochs=1000, 
                    validation_data=(test_images, test_labels),
                    callbacks=[tensorboard_callback],
                    use_multiprocessing=True,
                    workers=10,
                    batch_size=100)

model.summary()
model.save(filepath="./model",
           overwrite=True)