import logging
logging.getLogger("tensorflow").setLevel(logging.DEBUG)

from datetime import datetime
from packaging import version
import numpy as np

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import datasets, layers, models

assert float(tf.__version__[:3]) >= 2.3

config = tf.compat.v1.ConfigProto(gpu_options = 
                         tf.compat.v1.GPUOptions(per_process_gpu_memory_fraction=0.8)
# device_count = {'GPU': 1}
)
config.gpu_options.allow_growth = True
session = tf.compat.v1.Session(config=config)
tf.compat.v1.keras.backend.set_session(session)

# Load MNIST dataset
mnist = tf.keras.datasets.mnist
(train_images, train_labels), (test_images, test_labels) = mnist.load_data()

# Normalize the input image so that each pixel value is between 0 to 1.
train_images = train_images.astype(np.float32)/255.
test_images = test_images.astype(np.float32)

flat_images = []

for i in range(0, len(test_images)):
  image = []
  for j in range(0, len(test_images[0])):
    for k in range(0, len(test_images[0][0])):
      image.append(test_images[i][j][k])

  numpyImage = np.asarray(image)
  flat_images.append(numpyImage)
  print("image_", i, " = ", numpyImage.astype(np.uint8).tolist(), sep="")

np_flat_images = np.asarray(flat_images)

# Define the model architecture
model = tf.keras.Sequential([
  tf.keras.layers.Dense(6, input_shape=(784,), name="FC_H1"),
  tf.keras.layers.Dense(12, name="FC_H2"),
  tf.keras.layers.Dense(10, name="Output")
])

# Train the digit classification model
model.compile(optimizer='adam',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(
                  from_logits=True),
              metrics=['accuracy'])

logdir="logs/fit/" + datetime.now().strftime("%Y%m%d  -%H%M%S")
tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir)

model.fit(
  np_flat_images, # THIS IS THE DEFINITION OF OVERTRAINING NEVER DO THIS
  test_labels, # THIS IS THE DEFINITION OF OVERTRAINING NEVER DO THIS
  epochs=500,
  batch_size=10000,
  callbacks=[tensorboard_callback],
  validation_data=(np_flat_images, test_labels) # THIS IS THE DEFINITION OF OVERTRAINING NEVER DO THIS
)

model.summary()
model.save(filepath="./model",
  overwrite=True
)

def representative_data_gen():
  for input_value in tf.data.Dataset.from_tensor_slices(train_images).batch(1).take(100):
    yield [input_value]

converter = tf.lite.TFLiteConverter.from_keras_model(model)

tflite_model = converter.convert()

converter = tf.lite.TFLiteConverter.from_keras_model(model)
converter.optimizations = [tf.lite.Optimize.DEFAULT]
converter.representative_dataset = representative_data_gen
# Ensure that if any ops can't be quantized, the converter throws an error
converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS_INT8]
# Set the input and output tensors to uint8 (APIs added in r2.3)
converter.inference_input_type = tf.uint8
converter.inference_output_type = tf.uint8

tflite_model_quant = converter.convert()

interpreter = tf.lite.Interpreter(model_content=tflite_model_quant)
input_type = interpreter.get_input_details()[0]['dtype']
print('input: ', input_type)
output_type = interpreter.get_output_details()[0]['dtype']
print('output: ', output_type)

import pathlib

tflite_models_dir = pathlib.Path("./mnist_tflite_models/")
tflite_models_dir.mkdir(exist_ok=True, parents=True)

# Save the unquantized/float model:
tflite_model_file = tflite_models_dir/"mnist_model.tflite"
tflite_model_file.write_bytes(tflite_model)
# Save the quantized model:
tflite_model_quant_file = tflite_models_dir/"mnist_model_quant.tflite"
tflite_model_quant_file.write_bytes(tflite_model_quant)