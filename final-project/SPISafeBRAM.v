module SPISafeBRAM(
  input readClock,
  input writeClock,
  input writeEnable,
  input [9:0] writeAddress,
  input [7:0] writeData,
  input [9:0] readAddress,
  input writeSelect,

  output [7:0] readData
);

reg writeEnable_1;
reg writeEnable_2;

wire [7:0] ramOut_1;
wire [7:0] ramOut_2;
wire [7:0] readOut;

SPIBRAM ram1 (
  .rdclock(readClock),
  .wrclock(writeClock),
  .wren(writeEnable),
  .wraddress(writeEnable_1),
  .data(writeData),
  .rdaddress(readAddress),
  .q(ramOut_1)
);

SPIBRAM ram2 (
  .rdclock(readClock),
  .wrclock(writeClock),
  .wren(writeEnable),
  .wraddress(writeEnable_2),
  .data(writeData),
  .rdaddress(readAddress),
  .q(ramOut_2)
);

SPIMux outputMux (
  .clock(readClock),
  .data0x(ramOut_1),
  .data1x(ramOut_2),
  .sel(writeSelect),
  .result(readOut)
);

always @(posedge writeClock) begin
  writeEnable_1 <= writeEnable & ~writeSelect;
  writeEnable_2 <= writeEnable &  writeSelect;
end 

endmodule 