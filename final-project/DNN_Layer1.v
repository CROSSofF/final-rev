module DNN_Layer1(
  input a_reset,
  input clock,
  input [7:0] neuronInVal,
  input [9:0] neuronInPos,
  
  output reg [7:0] neuronOutVal,
  output reg [2:0] neuronOutPos
)

reg [10:0] counter;
reg [7:0] weights [5:0];

NN1_1W neuronWeight1_1 (
  .clock(clock),
  .address(neuronInPos),
  .q(weights[0])
)

MultAcc neuronAcc (
  .aclr3(a_reset),
  .clock0(clock),
  .dataa(neuronInVal)
)

always @(posedge clock) begin
  if (counter == 10'd784) begin // Weights + bias
  
  end
end

endmodule 