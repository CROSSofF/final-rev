module FinalProject (
  input a_reset_n,
  input clock,     
  input SCLK, 
  input MOSI,  
  input SSEL_n, 

  output hsync,         
  output vsync,            
  output [7:0] vgaGrey 
);

/* Clock setup
 Derive a 25.175MHz clock, gated by the state of the PLL. If the
 PLL is unstable, the pixel clock is disabled and this module is
 reset.
*/
wire pixelClock;
wire locked;

wire gatedPixelClock;
assign gatedPixelClock = pixelClock & locked;

VGAClock pixelClock_module(
  .areset(a_reset),
  .inclk0(clock),
  .c0(pixelClock),
  .locked(locked)
);

wire [9:0] pixelAddress;
wire [7:0] pixelValue;

VGADriver imageDriver (
  .pixelClock(gatedPixelClock),
  .a_reset(a_reset),
  .pixelAddress(pixelAddress),
  .hsync(hsync),
  .vsync(vsync)
);

//=========================================================

wire a_reset = ~a_reset_n; // Active low to active high reset

wire [7:0] fifoWriteData; // Data to write to FIFO
wire fifoWriteRequest;    // Request write to FIFO

wire fifoEmpty;                    // FIFO has no data
reg fifoEmpty_n;                   // FIFO has data

wire [7:0] fifoReadData;           // Data read from FIFO
reg fifoReadRequest;               // If data in FIFO, read from FIFO

wire ramToggle;

reg [9:0] spiByteCounter;
localparam MAX_BYTES = 10'd784;

SPISlave spi (
  .clock(clock),
  .a_reset(a_reset),
  .SSEL_n(SSEL_n),
  .SCLK(SCLK),
  .MOSI(MOSI),
  .byte(fifoWriteData),
  .byteReady(fifoWriteRequest),
  .ramSelect(ramToggle)
);

SPIFIFO spiBuffer (
  .aclr(a_reset),
  .clock(clock),
  .data(fifoWriteData),
  .wrreq(fifoEmpty_n),
  .q(fifoReadData),
  .rdreq(fifoReadRequest),
  .empty(fifoEmpty),
  .full()                  // FIFO full state unused
);

SPISafeBRAM vgaRAM (
  .readClock(pixelClock),
  .readAddress(pixelAddress),
  .readData(pixelValue),
  .writeClock(clock),
  .writeEnable(fifoReadRequest),
  .writeAddress(spiByteCounter),
  .writeData(fifoReadData),
  .writeSelect(ramToggle),
);

always @(clock) begin
  fifoEmpty_n <= ~fifoEmpty;      // Set FIFO not empty
  fifoReadRequest <= ~fifoEmpty;  // Set read flag if FIFO not empty
end

always @(posedge ramToggle or posedge fifoWriteRequest) begin
  if (ramToggle) begin
    spiByteCounter <= 10'b0;
  end 
  else if (fifoWriteRequest) begin
    if (spiByteCounter == MAX_BYTES - 1) begin
      spiByteCounter <= 10'b0;
    end
    else begin
      spiByteCounter <= spiByteCounter + 1'b1;
    end
  end
end

//========================================================





endmodule 