module SPISlave(
  input a_reset,
  input clock,
  input SSEL_n,
  input SCLK,
  input MOSI,
  
  output reg [7:0] byte,
  output reg byteReady,
  output reg ramSelect
);

reg [6:0] tempByte;   // Temporary storage for bits read in 
reg [3:0] bitCounter; // Counts bits received

reg [2:0] sckState;   // Stored SCK to detect edges with 1 clock cycle of debounce

always @(posedge SSEL_n) begin
  
end

always @(posedge a_reset or posedge clock) begin
  if (a_reset) begin
    
  end
  else if (clock) begin
    if (SSEL_n) begin                        // Slave has been selected
      if (sckState == 3'b011) begin            // Clock rising edge detected
        if (bitCounter == 4'd7) begin            // Rising edge detected 8th bit
          byte <= {tempByte, MOSI};                // Send byte
          byteReady <= 1'b1;                       // Signal byte is ready
          
          bitCounter <= 5'b0;                      // Reset counter
        end
        else begin                               // Not 8th bit detected
          tempByte <= {tempByte[5:0], MOSI};       // Shift MSB left
          bitCounter <= bitCounter + 1'b1;         // Increment bit counter
        end
      end
      
      sckState <= {sckState[1:0], SCLK};       // Update edge detector
      byteReady <= 0;
      
      ramSelect <= 0;
    end
    else begin                               // Slave disabled
      bitCounter <= 5'b0;                      // Reset bit counter
      sckState <= 3'b0;                        // Reset edge detector
      ramSelect <= 1;
    end
  end
end

endmodule 