/*
 Neural Network (see report for details)
 
 A pre-trained convolutional neural network to analyse images from the
 MNIST database of handwritten digits.
 
 Weight multiplication for non-convolutional layers are time multiplexed.
 Bias subtractions are time multiplexed. Max pooling operations are 
 completed in 1 clock cycle
 
 [digit] is guaranteed to be accurate when [ready] is high. [digit]
 is held at 4'b1111 when [ready] is low.
 
 Structure:
   ________________________________________________
   Layer (type)                 Output Shape       
   ================================================
   max_pooling2d (MaxPooling2D) (None, 14, 14, 1)  
   ________________________________________________
   conv2d (Conv2D)              (None, 12, 12, 12) 
   ________________________________________________
   max_pooling2d_1 (MaxPooling2 (None, 3, 3, 12)   
   ________________________________________________
   dense (Dense)                (None, 10)         
   ________________________________________________
   
   
 
*/
module NeuralNetwork(
  input pixelValue,
  input pixelX,
  input pixelY,
  
  output pixelAddress,
  output [3:0] digit,
  output ready
);

reg imageBuffer[27:0][27:0];

endmodule 