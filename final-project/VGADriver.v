/* 
 VGA Driver (28x28 scaled to 640x480 @ 60Hz)
 
 Pixel Clock @ 25.175MHz
 Image Size @ 28x28
 
 inputs:
   pixelClock - 25.175MHz clock from PLL (reference for PLL)
   a_reset - asynchronous reset
   pixelValue - 8-bit grayscale pixel value (read in from BRAM).
 
 outputs:
   pixelAddress - address of the next requested pixel, compensated 
                  by 2 clock cycles to account for delays in the 
                  BRAM module.
   hsync - horizontal sync signal (see VGA spec)
           active low
   vsync - vertical sync signal (see VGA spec)   
           active low
*/ 
module VGADriver(
  input pixelClock, 
  input a_reset,
  
  output reg [9:0] pixelAddress,
  output reg hsync,
  output reg vsync
);

/* 
 VGA Constants (http://tinyvga.com/vga-timing/640x480@60Hz) 
 
 Image Original Size  = 28x28
 Pixel Scaling Factor = x16
 
 Image Display Size = 448x448 
 Horizontal Padding = 76 pixels
 Vertical Padding   = 16 pixels

 Number of scanlines = 525 (480 displayed)
 Values per scanline = 800 (600 displayed)
 
 Horizontal Constants:
   Front Porch = 16 pixels
   Back Porch  = 48 pixels
   Sync Pulse  = 96 pixels
   
 Vertical Constants:
   Front Porch = 10 pixels 
   Back Porch  = 33 pixels
   Sync Pulse  =  2 pixels
 
 Horizontal Blanking Interval = 352 pixels (800 - 448)
 Vertical Blanking Interval   =  77 pixels (525 - 448)

 Below:
   (1) - Trailing Offset:
           Used to centre the image (not part of the official spec).
           Display black here -> blank.
   (2) - Back Porch:
           Prepare for sync period -> blank.
   (3) - Sync Period
           Sends the raster beam to the beginning of the next line.
           Moving raster beam -> blank.
   (4) - Front Porch:
           Allows raster beam velocity to stabilise. Raster beam
           decelerating -> blank.
   (5) - Leading Offset:
           See (1)

 Diagram (not to scale - blanking is actually significantly shorter 
          than image duration):

 High                     ────────────────────────────────────────────────────────────────┐     ┌────────────
                  H-sync                                                                  │     │               
     Low                                                                                  └─────┘
                                                                                    Horizontal Blanking
                                                                              ┌─────────────────────────────┐
 V-sync                                           Pixels                        (1)   (2)   (3)   (4)   (5) 
                          ┌───────────────────────────────────────────────────┬─────┬─────┬─────┬─────┬─────┐
  │                     ┌ ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
  │                     │ ┃                                                   ┃                             ┃
  │                     │ ┃                                                   ┃                             ┃
  │                     │ ┃                                                   ┃                             ┃
  │                     │ ┃                                                   ┃                             ┃
  │                     │ ┃                                                   ┃                             ┃
  │                     │ ┃                                                   ┃                             ┃
  │                     │ ┃                                                   ┃                             ┃
  │                     │ ┃                      Image                        ┃                             ┃
  │              Pixels │ ┃          (pixelValue = *pixelAddress)             ┃                             ┃
  │                     │ ┃                                                   ┃                             ┃
  │                     │ ┃                                                   ┃                             ┃
  │                     │ ┃                                                   ┃                             ┃
  │                     │ ┃                                                   ┃                             ┃
  │                     │ ┃                                                   ┃                             ┃
  │                     │ ┃                                                   ┃                             ┃
  │                     │ ┃                                                   ┃                             ┃
  │                     │ ┃                                                   ┃                             ┃
  │               ┌     ├ ┣━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛                             ┃
  │               │ (1) │ ┃                                                                                 ┃
  │               │     ├ ┃                                                                                 ┃
  │               │ (2) │ ┃                                                                                 ┃
  └───┐           │     ├ ┃                                                              Blanking           ┃
      │ Verticcal │ (3) │ ┃                                                        (pixelValue = 0xFF)      ┃
  ┌───┘ Blanking  │     ├ ┃                                                                                 ┃
  │               │ (4) │ ┃                                                                                 ┃
  │               │     ├ ┃                                                                                 ┃
  │               │ (5) │ ┃                                                                                 ┃
  │               └     └ ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
*/

localparam ROW_COUNT = 10'd525; // Total number of scanlines
localparam COL_COUNT = 10'd800; // Values per scanline

localparam ORIGINAL_IMAGE_WIDTH  = 10'd28;
localparam ORIGINAL_IMAGE_HEIGHT = 10'd28;
localparam SCALING_FACTOR        = 10'd16;
localparam SCALED_IMAGE_WIDTH    = 10'd448; // Scaled image width  (16x28)
localparam SCALED_IMAGE_HEIGHT   = 10'd448; // Scaled image height (16x28)
localparam X_OFFSET              = 10'd76; // Horizontal padding ((600-448)/2)
localparam Y_OFFSET              = 10'd16; // Vertical padding   ((525-448)/2)

localparam H_FRONT_PORCH = 10'd16; // Horizontal front porch duration
localparam H_SYNC        = 10'd96; // Horizontal sync duration
localparam H_BACK_PORCH  = 10'd48; // Horizontal back porch

localparam V_FRONT_PORCH = 10'd10; // Vertical front porch duration
localparam V_SYNC        = 10'd2;  // Verical sync duration
localparam V_BACK_PORCH  = 10'd33; // Vertical back porch duration

localparam H_SYNC_START = SCALED_IMAGE_WIDTH + X_OFFSET + H_BACK_PORCH;
localparam H_SYNC_END   = H_SYNC_START + H_SYNC;

localparam V_SYNC_START = SCALED_IMAGE_HEIGHT + Y_OFFSET + V_BACK_PORCH;
localparam V_SYNC_END   = V_SYNC_START + V_SYNC;

localparam NOT_AN_ADDRESS = 10'b11_1111_1111;

/* Counters

 Counters for raster pixel and image pixel. Counters with the "_future" 
 suffix are used to fetch pixel values from BRAM. Counters
 without a suffix are used to control hsync and vsync signals.

*/
reg [9:0] rasterX;
reg [9:0] rasterY;

//=============================================================

always @(posedge pixelClock) begin
  if (rasterX == COL_COUNT - 1) begin
    // End of scanline -> Reset rasterX counter
    rasterX <= 0;
    
    if (rasterY == ROW_COUNT - 1) begin
      // End of image -> Reset rasterY counter
      rasterY <= 0;
    end
    else begin
      // Increment rasterY
      rasterY <= rasterY + 1'b1; 
    end
  end
  else begin
    // Generate hsync
    if (rasterX >= H_SYNC_START && rasterX < H_SYNC_END) begin
      hsync <= 0;
    end
    else begin
      hsync <= 1;
    end

    // Increment rasterX 
    rasterX <= rasterX + 1'b1;
  end

  // Generate vsync
  if (rasterY == V_SYNC_START && rasterY < V_SYNC_END) begin
    vsync <= 0;
  end
  else begin
    vsync <= 1;
  end

  // Calculate pixelAddress
  if (rasterX < SCALED_IMAGE_WIDTH && rasterY < SCALED_IMAGE_HEIGHT) begin
    // Pixel coordinates
    pixelAddress <= (rasterY / SCALING_FACTOR) * ORIGINAL_IMAGE_WIDTH
                  + (rasterX / SCALING_FACTOR);
  end
  else begin
    // Out of bounds -> blanking interval
    pixelAddress <= NOT_AN_ADDRESS;
  end
end

endmodule 