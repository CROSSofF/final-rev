module wrapper(
  input clock,
  input SPI_CLK,
  input SPI_MOSI,
  input SPI_nSS,
  output [7:0] dataOut
);

  localparam readDivider = 25000000;

  wire [7:0] data;
  wire dataReady;
  
  reg [3:0] readCounter;
  reg [3:0] writeCounter;
  reg [27:0] clockCounter;
  
  always @(posedge clock) begin
    if (clockCounter == (readDivider - 1)) begin
      clockCounter <= 0;
      readCounter <= readCounter + 1;
    end
    
    clockCounter <= clockCounter + 1;
  end
  
  always @(posedge dataReady) begin
    writeCounter <= writeCounter + 1;
  end

  ram ram(
    .data(data),
    .rdclock(clock),
    .rdaddress(readCounter),
    .wrclock(dataReady),
    .wraddress(writeCounter),
    .wren(1),
    .q(dataOut)
  );
  
  spi_slave slave(
    .clock(clock),
    .SPI_CLK(SPI_CLK),
    .SPI_MOSI(SPI_MOSI),
    .SPI_nSS(SPI_nSS),
    .data(data),
    .dataReady(dataReady)
  );

endmodule 