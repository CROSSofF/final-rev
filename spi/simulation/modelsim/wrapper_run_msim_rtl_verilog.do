transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+D:/git/spi {D:/git/spi/wrapper.v}
vlog -vlog01compat -work work +incdir+D:/git/spi {D:/git/spi/spi_slave.v}
vlog -vlog01compat -work work +incdir+D:/git/spi {D:/git/spi/ram.v}

