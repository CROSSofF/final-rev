module spi_slave(
  input clock,
  input SPI_CLK,
  input SPI_MOSI,
  input SPI_nSS,
  output reg [7:0] data,
  output reg dataReady
);

reg [7:0] shift_MOSI;

reg [2:0] counter;
reg shift_counter0;

always @(posedge clock) begin
  if (counter == 3'b000 && shift_counter0 == 1'b1) begin
    data <= shift_MOSI;
    dataReady <= 1;
  end
  else begin
    dataReady <= 0;
  end
end

always @(posedge SPI_CLK) begin
  if (SPI_nSS == 0) begin              // SS low -> slave selected
    shift_MOSI <= { shift_MOSI[6:0], SPI_MOSI };
    shift_counter0 <= counter[0];
    counter <= counter + 1; 
  end
  else if (SPI_nSS == 1) begin                           // SS high -> slave not selected
    counter <= 3'b000;
    shift_counter0 <= 0; 
  end
end

endmodule 