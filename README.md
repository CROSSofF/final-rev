# Final Project #

Git repository for final project.

### Structure ###

The main project is contained within `/final-project`

The convolver module, which is unused in the final project but still referenced, is contained within `/convolver`

The TensorFlow neural network code is contained within `/mnist-nn`

#### Viewing Neural Networks ####

If the TensorBoard Python module is downloaded, the logs for all training can be viewed by running the command:
`python -m tensorboard.main --logdir logs`
from `/mnist-nn`, then navigatingg to `https://localhost:6006`. NOTE: TensorFlow and TensorBoard are not compatible with Python 32-bit