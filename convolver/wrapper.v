module wrapper(
  input reset,
  input clock,
  output reg [8:0] gx,
  output reg [8:0] gy,
  output reg [8:0] g,
  
  output reg gx_ready,
  output reg gy_ready,
  output convReset
);
  reg [4:0] counter;
  reg first;
  
  reg [4:0] shift_counter[2:0];
  reg [2:0] shift_reset;
  assign convReset = shift_reset[2] || (shift_counter[2] == 12) || reset;
  
  reg [6:0] pixelX;
  reg [6:0] pixelY;

  reg [4:0] kernelAddress;
  reg [12:0] pixelAddress;

  wire [8:0] gx_inBuffer;
  wire [8:0] gy_inBuffer;
  wire [7:0] pixel_inBuffer;
  
  wire [8:0] gx_outBuffer;
  wire [8:0] gy_outBuffer;
  
  wire [8:0] g_buffer;
  
  wire gx_readyBuffer;
  wire gy_readyBuffer;
  
  // Init kernel roms
  gx_kernel gx_kernel(
    kernelAddress,
    clock,
    gx_inBuffer
  );
  
  gy_kernel gy_kernel(
    kernelAddress,
    clock,
    gy_inBuffer
  );

  // Init image rom
  image_rom image(
    pixelAddress,
    clock,
    pixel_inBuffer
  );
  
  // Init colvolvers
  convolver gx_conv(
    convReset,
    clock,
    pixel_inBuffer,
    gx_inBuffer,
    gx_readyBuffer,
    gx_outBuffer
  );
  
  convolver gy_conv(
    convReset,
    clock,
    pixel_inBuffer,
    gy_inBuffer,
    gy_readyBuffer,
    gy_outBuffer
  );
  
  magnitude g_mag(
    gx,
    gy,
    g_buffer
  );
  
  always @(posedge clock) begin
    case (counter)                           
      5'b00000 : begin
        pixelAddress <= 40*(pixelY - 1) + (pixelX - 1); 
        kernelAddress <= 8;           
      end
      5'b00001 : begin
        pixelAddress <= 40*(pixelY - 1) + (pixelX);             
        kernelAddress <= 7;           
      end
      5'b00010 : begin
        pixelAddress <= 40*(pixelY - 1) + (pixelX + 1);            
        kernelAddress <= 6;        
      end
      5'b00011 : begin
        pixelAddress <= 40*(pixelY) + (pixelX - 1);     
        kernelAddress <= 5;        
      end
      5'b00100 : begin
        pixelAddress <= 40*(pixelY) + (pixelX);    
        kernelAddress <= 4;         
      end
      5'b00101 : begin
        pixelAddress <= 40*(pixelY) + (pixelX + 1);      
        kernelAddress <= 3;           
      end
      5'b00110 : begin
        pixelAddress <= 40*(pixelY + 1) + (pixelX - 1);       
        kernelAddress <= 2;          
      end
      5'b00111 : begin
        pixelAddress <= 40*(pixelY + 1) + (pixelX);      
        kernelAddress <= 1;         
      end
      5'b01000 : begin
        pixelAddress <= 40*(pixelY + 1) + (pixelX + 1);     
        kernelAddress <= 0;         
      end
      default : begin
        pixelAddress <= 0;               // RESET
        kernelAddress <= 8;
      end
    endcase
    
    if (reset) begin
      counter <= 0;
      first <= 1;
      gx <= 0;
      gy <= 0;
      pixelX <= 7'b1;
      pixelY <= 7'b1;
    end 
    else if ((counter == 12))begin
      counter <= 0;
      
      if (pixelX == 38) begin
        pixelX <= 7'b1;
      
        if (pixelY == 38) begin
          pixelY <= 7'b1;
        end
        else begin
          pixelY <= pixelY + 1;
        end
      end
      else begin
        pixelX <= pixelX + 1;
      end
    end
    else begin
      counter <= counter + 1;
      gx <= gx_outBuffer;
      gy <= gy_outBuffer;
    end
    
    gx_ready <= gx_readyBuffer;
    gy_ready <= gy_readyBuffer;
    g <= g_buffer;
    
    shift_reset <= { shift_reset[1:0], reset};
    shift_counter[2] <= shift_counter[1];
    shift_counter[1] <= shift_counter[0];
    shift_counter[0] <= counter;
  end

endmodule 