import argparse
import cv2

parser = argparse.ArgumentParser(description="Extracts byte data from an image file and puts it into a .mif file for RAM/ROM usage.")

ioGroup = parser.add_argument_group("I/O Options")
ioGroup.add_argument("-i", "--input-file", dest="inputFile", required=True,
                    help="input image file path")
ioGroup.add_argument("-o", "--output-file", dest="outputFile", required=True,
                    help="destination file path")

subimageGroup = parser.add_argument_group("Sub-image Options")
subimageGroup.add_argument("-x1", type=int, dest="x1",
                    help="top left x coordinate")
subimageGroup.add_argument("-y1", type=int, dest="y1",
                    help="top left y coordinate")
subimageGroup.add_argument("-x2", type=int, dest="x2",
                    help="bottom right x coordinate")
subimageGroup.add_argument("-y2", type=int, dest="y2",
                    help="bottom right y coordinate")

args = vars(parser.parse_args())
print(args)

image = cv2.imread(args.get("inputFile"))

counter = 0
x1 = args.get("x1")
y1 = args.get("y1")
x2 = args.get("x2")
y2 = args.get("y2")

if (x1 != None):
  counter += 1
if (y1 != None):
  counter += 1
if (x2 != None):
  counter += 1
if (y2 != None):
  counter += 1

if counter > 1 and counter < 4:
  raise argparse.ArgumentTypeError

rows, cols, length = image.shape

def genFile(filePath, x1=0, y1=0, x2=cols, y2=rows):
  outFile = open(filePath, "w+")
  outFile.writelines([
    "DEPTH = "+str((x2-x1)*(y2-y1))+";                   -- The size of memory in words\n",
    "WIDTH = 8;                    -- The size of data in bits\n",
    "ADDRESS_RADIX = HEX;          -- The radix for address values\n",
    "DATA_RADIX = HEX;             -- The radix for data values\n",
    "CONTENT                       -- start of (address : data pairs)\n",
    "BEGIN\n",
    "\n"
  ])

  memAddress = 0
  for x in range(x1, x2):
    for y in range(y1, y2):
      line = hex(memAddress)[2:]+" : "
      line += hex(image[x, y, 0])[2:]
      line += ";\n"
      outFile.write(line)

      memAddress += 1

  outFile.writelines([
    "\n",
    "END\n"
  ])
  outFile.close()



output = args.get("outputFile")
if counter == 0:
  genFile(output)
else:
  genFile(output, x1, y1, x2, y2)