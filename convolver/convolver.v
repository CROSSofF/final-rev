
//          | p_0 p_1 p_2 |              
// Pixels = | p_3 p_4 p_5 |
//          | p_6 p_7 p_8 |

//
//          | k_0 k_1 k_2 |              
// Kernel = | k_3 k_4 k_5 |
//          | k_6 k_7 k_8 |
//
module convolver(
  input reset,
  input clock,
  input [7:0] pixel,
  input [8:0] kernel,
  output reg dataReady,
  output reg [8:0] data
);

parameter kernelWidth = 3;
parameter kernelHeight = 3;

localparam maxCount = (kernelWidth*kernelHeight + 3); // Reg A + Reg Mul + Reg Out - 0th

reg [4:0] counter;
wire [16:0] sum;

wire clearMAC;

mac convolver(
  clearMAC,
  clock,
  pixel,
  kernel,
  sum
);

assign clearMAC = (counter == maxCount) || reset;

// -------------------------------------------------------------------

always @(posedge clock) begin
  if (reset) begin
    data <= 0;
    dataReady <= 0;
    counter <= 0;
  end
  else if (counter == 11) begin
//    if (sum[8:0] < -9'd255) 
//      data <= -9'd255;
//    else if (sum[8:0] > 9'd255)  
//      data <= 9'd255;
//    else 
      data <= sum[8:0];
     
    dataReady <= 1;
  end
  else begin
    dataReady <= 0;
    counter <= counter + 1;
  end
end

endmodule 