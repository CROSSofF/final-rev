module magnitude(
  input [8:0] gx,
  input [8:0] gy,
  output [8:0] g
);

wire [17:0] radical;
wire [17:0] gx_squared;
wire [17:0] gy_squared;

assign radical = gx_squared + gy_squared;

square gx2(
  .dataa(gx),
  .result(gx_squared)
);

square gy2(
  .dataa(gy),
  .result(gy_squared)
);

sqrt sqrt(
  .radical(radical),
  .q(g),
  .remainder()
);

endmodule 